import * as defaultActions from '../actions/default'
import { listTuple, dispatch as init, actor } from '../util/core'

const actions = new Map([...listTuple(defaultActions)])

export const action = actor(actions)

export const dispatch = init(actions)
