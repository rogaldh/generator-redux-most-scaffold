import { combineReducers } from 'redux'

import * as def from '../reducers/default'
import { selectReducer } from '../util/core'

export default combineReducers({
  'default': selectReducer(def, { data: [], message: '' }),
})
