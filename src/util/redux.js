import { compose as legacyCompose } from 'redux'

// Sets up a Chrome extension for time travel debugging.
// See https://github.com/zalmoxisus/redux-devtools-extension for more information.
export const compose = (fn = legacyCompose, _global, options = {}) => typeof _global === 'object' && _global.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? _global.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(options)
  : fn
