const lower = name => name.toLocaleLowerCase()

export const upper = name => name.toLocaleUpperCase()

export const list = _ => Object.keys(_).map(it => _[it])

export const listTuple = hashmap => Object
  .keys(hashmap)
  .reduce((acc, actionName) => acc
    .concat([[upper(actionName), hashmap[actionName]]]), [])

export const actor = actions => (actionName, payload) => {
  const type = upper(actionName)
  const action = actions.get(type)
  if (!action) throw new TypeError(`Can not get the '${type}' action`)

  return action({ type, payload })
}

export const dispatch = (actions) => {
  const actAs = actor(actions)

  return (context, type, payload) => context.props.dispatch(actAs(type, payload))
}

export const selectReducer = (reducers, initState = {}) => (nextState, action) => {
  const state = nextState || initState
  const name = lower(action.type)

  return (reducers[name] || (x => x))(state, action.payload)
}
