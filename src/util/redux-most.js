import { concatMap } from 'most/src/combinator/concatMap'
import { delay } from 'most/src/combinator/delay'
import { filter } from 'most/src/combinator/filter'
import { flatMap } from 'most/src/combinator/flatMap'
import { merge } from 'most/src/combinator/merge'
import { switchLatest } from 'most/src/combinator/switch'
import { takeUntil } from 'most/src/combinator/timeslice'
import { tap, map } from 'most/src/combinator/transform'
import { throttle, debounce } from 'most/src/combinator/limit'
import compose from 'ramda/es/compose'
import curry from 'ramda/es/curry'

export const log = console.log // eslint-disable-line

export const tapLog = x => log(x) || x

const mapTo = (x, stream) => map(() => x, stream)
const switchMap = compose(switchLatest, map)

export const chainC = curry(flatMap)

export const concatMapC = curry(concatMap)

export const debounceC = curry(debounce)

export const delayC = curry(delay)

export const filterC = curry(filter)

export const mapC = curry(map)

export const mapToC = curry(mapTo)

export const mergeC = curry(merge)

export const switchMapC = curry(switchMap)

export const tapC = curry(tap)

export const throttleC = curry(throttle)

export const untilC = curry(takeUntil)

