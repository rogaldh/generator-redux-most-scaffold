import React from 'react'
import { holdSubject } from 'most-subject'
import { of, from, fromPromise, mergeArray, zip } from 'most'
import { cond, compose, is, prop, identity, isArrayLike, T, reduce, keys } from 'ramda'

const mirror = (list, transform = x => x) => list
  .reduce((a, e) => {
    a[e] = transform(e) // eslint-disable-line no-param-reassign

    return a
  }, {})

const actionNames = ['DEFAULT_ACTION']

const actor = actionType => (data, ...argv) => ({
  ...{ type: actionType },
  ...data,
  ...argv,
})

export const actions = mirror(actionNames, actor)

const reducer = (state, action) => ({ ...state, ...action })

export const reducers = {
  'DEFAULT_ACTION': reducer,
}

const update = (initialValue, [streams, effects]) => mergeArray(streams
  .map((stream, i) => {
    const resolve = (...values) => prev => effects[i](prev, ...values)

    return Array.isArray(stream) ? zip(resolve, ...stream) : stream.map(resolve)
  }))
  .scan((prev, f) => f(prev), initialValue)
  .multicast()

export const connect = (store, ...argv) => Children => class extends React.Component { // eslint-disable-line no-unused-vars, max-len
  constructor (props) {
    super(props)

    this.store = store.getStream()
  }
  componentWillMount () {
    this.store.observe(this.setState.bind(this))
  }
  render () {
    const data = {
      ...this.state,
      ...this.props,
    }

    return <Children {...data} {...argv} dispatch={store.dispatch} />
  }
}

/**
 * createStore
 *
 * ```js
 * import createStore, { connect, reducers } from './util/stream'
 * const store$ = createStore(reducers, Promise.resolve({}))
 *
 * const Component = connect(store$)(ReactComponent)
 * ```
 */
export default (reducer, initialValue = {}) => { // eslint-disable-line no-shadow
  const initialStream = cond([
    [compose(is(Function), prop('then')), fromPromise],
    [compose(is(Function), prop('observe')), identity],
    [isArrayLike, from],
    [T, of],
  ])(initialValue)

  const actionMap = compose(
    reduce((acc, actionName) => acc.set(actionName, holdSubject()), new Map()),
    keys,
  )(actions)

  const patterns = reduce((acc, each) => {
    const [key, { stream }] = each
    const concatProp = (i, x) => prop(i, acc).concat(x)

    return [concatProp(0, stream), concatProp(1, reducer[key])]
  }, [[], []], actionMap)

  return {
    getStream: () => initialStream.concatMap(v => update(v, patterns)),
    /**
     * @function {function dispatch}
     * @param  {String} actionName
     * @param  {Array<Object>} ...payload
     * @return {void}
     */
    dispatch: (actionName, ...payload) => {
      actionMap.get(actionName).observer.next(actions[actionName](...payload))
    },
  }
}
