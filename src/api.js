let client = null // eslint-disable-line no-unused-vars
let opts = {}

const mergeOptions = options => ({ // eslint-disable-line no-unused-vars
  ...opts,
  ...options,
})

export const set = (cli, opt) => {
  if (cli) client = cli
  if (opt) opts = opt
}
