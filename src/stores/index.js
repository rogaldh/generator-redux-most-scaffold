import composeR from 'ramda/es/compose'
import { createStore, applyMiddleware } from 'redux'
import { createEpicMiddleware, createStateStreamEnhancer } from 'redux-most'

import epics from '../epics/index'
import reducers from '../reducers/index'
import { compose as composeEnhancers } from '../util/redux'

const epicMiddleware = createEpicMiddleware(epics)
const compose = composeEnhancers(composeR, window)

const middlewares = []

export const store = createStore(
  reducers,
  compose(
    createStateStreamEnhancer(epicMiddleware),
    applyMiddleware(...middlewares),
  )
)

/**
 * ↓ Development only ↓
 */

// hot reload reducers
const replaceRootReducer = () => {
  import('../reducers/index')
    .then(({ 'default': nextRootReducer }) => store.replaceReducer(nextRootReducer))
    .catch(console.error) // eslint-disable-line no-console
}

if (module.hot) {
  module.hot.accept('../reducers/index', replaceRootReducer)
}

// hot reload epics
const replaceRootEpic = () => {
  const nextRootEpic = require('../epics/index').default // eslint-disable-line

  epicMiddleware.replaceEpic(nextRootEpic)
}

if (module.hot) {
  module.hot.accept('../epics/index', replaceRootEpic)
}

/**
 * ↑ End of Development only ↑
 */
