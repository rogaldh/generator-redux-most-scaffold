import compose from 'ramda/es/compose'
import { select } from 'redux-most'
import { fromPromise } from 'most/src/combinator/promises'

import { action } from '../actions/index'
import { upper } from '../util/core'

import {
  debounceC as debounce, // eslint-disable-line no-unused-vars
  filterC as filter, // eslint-disable-line no-unused-vars
  mapC as map,
  switchMapC as switchMap,
  tapLog, // eslint-disable-line no-unused-vars
} from '../util/redux-most'

const make_request = x => Promise.resolve(x)

export const should_do_something = compose(
  switchMap(__ => compose(
    map(res => action('do_something', res)),
    q => fromPromise(make_request({
      data: q,
    }))
  )(__)), // eslint-disable-line function-paren-newline
  map(({ payload }) => payload),
  select(upper('should_do_something'))
)
