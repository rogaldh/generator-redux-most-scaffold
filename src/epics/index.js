import { combineEpics } from 'redux-most'

import * as defaultEpics from '../epics/default'
import { list } from '../util/core'

export default combineEpics([...list(defaultEpics)])
